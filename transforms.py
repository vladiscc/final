import images

def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    imagen = []
    for lista in image:
        nueva_lista = []
        for pixel in lista:
            if pixel == to_change:
                pixel = to_change_to
            nueva_lista.append(pixel)
        imagen.append(nueva_lista)
    return imagen

def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])
    rotated_image = []
    for i in range(cols):
        rotated_image.append([])
        for j in range(rows):
            rotated_image[i].append(image[j][-i])
    return rotated_image

def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    image.reverse()
    return image

def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    rotated_colours = images.create_blank(width, height)
    for x in range(width):
        for y in range(height):
            r = image[x][y][0]
            g = image[x][y][1]
            b = image[x][y][2]
            r = (r + increment) % 256
            g = (g + increment) % 256
            b = (b + increment) % 256
            rotated_colours[x][y] = (r, g, b)
    return rotated_colours
def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    blurred_image = images.create_blank(width, height)
    for x in range(width):
        for y in range(height):
            total_r = 0
            total_g = 0
            total_b = 0
            pixels = 0
            for i in range(max(0, x-1), min(width, x+2)):
                for j in range(max(0, y-1), min(height, y+2)):
                    r, g, b = image[i][j]
                    total_r += r
                    total_g += g
                    total_b += b
                    pixels += 1
            average_r = total_r // pixels
            average_g = total_g // pixels
            average_b = total_b // pixels
            blurred_image[x][y] = (average_r, average_g, average_b)
    return blurred_image
def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    shifted_image = images.create_blank(width, height)
    for x in range(width):
        for y in range(height):
            new_x = (x + horizontal) % width
            new_y = (y - vertical) % height
            shifted_image[new_x][new_y] = image[x][y]
    return shifted_image

def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])
    x_end = min(x + width, cols)
    y_end = min(y + height, rows)
    cropped_image = []
    for i in range(y, y_end):
        if 0 <= i < rows:
            cropped_image.append([])
            for j in range(x, x_end):
                if 0 <= j < cols:
                    cropped_image[i - y].append(image[i][j])
    return cropped_image

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    grayscale = [[(sum(image[x][y])//3,)*3 for y in range(height)] for x in range(width)]
    return grayscale
def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])
    filtered_image = []
    for i in range(rows):
        filtered_image.append([])
        for j in range(cols):
            new_red = min(int(image[i][j][0] * r), 255)
            new_green = min(int(image[i][j][1] * g), 255)
            new_blue = min(int(image[i][j][2] * b), 255)
            filtered_image[i].append((new_red, new_green, new_blue))
    return filtered_image

