import sys
from transforms import *
from images import read_img, write_img
from PIL import Image

def split_filename_extension(nombre_imagen):
    parts = nombre_imagen.rsplit('.', 1)
    if len(parts) > 1:
        filename, file_extension = parts
        file_extension = '.' + file_extension
    else:
        filename = nombre_imagen
        file_extension = ''
    return filename, file_extension

def main():
    nombre_imagen = sys.argv[1]
    funcion = sys.argv[2]

    filename, file_extension = split_filename_extension(nombre_imagen)
    print(f"Nombre del archivo: {filename}")
    print(f"Extensión del archivo: {file_extension}")

    img = read_img(nombre_imagen)

    if funcion == 'change_colors':
        uno = int(sys.argv[3])
        dos = int(sys.argv[4])
        tres = int(sys.argv[5])
        cuatro = int(sys.argv[6])
        cinco = int(sys.argv[7])
        seis = int(sys.argv[8])
        a = change_colors(img, (uno, dos, tres), (cuatro, cinco, seis))

    elif funcion == 'rotate_colors':
        increment = int(sys.argv[3])
        a = rotate_colors(img, increment)

    elif funcion == 'shift':
        horizontal = int(sys.argv[3])
        vertical = int(sys.argv[4])
        a = shift(img, horizontal, vertical)

    elif funcion == 'crop':
        x = int(sys.argv[3])
        y = int(sys.argv[4])
        width = int(sys.argv[5])
        height = int(sys.argv[6])
        a = crop(img, x, y, width, height)

    elif funcion == 'filter':
        r = float(sys.argv[3])
        g = float(sys.argv[4])
        b = float(sys.argv[5])
        a = filter(img, r, g, b)


    else:
        print('Introduzca una de las funciones sugeridas: \n'
              '- change_colors\n'
              '- rotate_colors\n'
              '- shift\n'
              '- crop\n'
              '- filter\n')
        return

    write_img(a, f"{filename}_trans{file_extension}")
    print(f"Transformación aplicada: {funcion}")
    print(f"Imagen resultante guardada como: {filename}_trans{file_extension}")
    Image.open(f"{filename}_trans{file_extension}").show()

if __name__ == '__main__':
    main()