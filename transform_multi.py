import sys
from transforms import *
from images import read_img, write_img
from PIL import Image

def split_filename_extension(nombre_imagen):
    parts = nombre_imagen.rsplit('.', 1)
    if len(parts) > 1:
        filename, file_extension = parts
        file_extension = '.' + file_extension
    else:
        filename = nombre_imagen
        file_extension = ''
    return filename, file_extension

def main():
    nombre_imagen = sys.argv[1]
    contador = 2
    filename, file_extension = split_filename_extension(nombre_imagen)
    print(f"Nombre del archivo: {filename}")
    print(f"Extensión del archivo: {file_extension}")

    img = read_img(nombre_imagen)

    while True:

        funcion = sys.argv[contador]
        contador += 1

        if funcion == 'rotate_right':
            img = rotate_right(img)
        elif funcion == 'mirror':
            img = mirror(img)
        elif funcion == 'blur':
            img = blur(img)
        elif funcion == 'grayscale':
            img = grayscale(img)

        elif funcion == 'change_colors':
            uno = int(sys.argv[contador])
            dos = int(sys.argv[contador+1])
            tres = int(sys.argv[contador+2])
            cuatro = int(sys.argv[contador+3])
            cinco = int(sys.argv[contador+4])
            seis = int(sys.argv[contador+5])
            contador += 6
            img = change_colors(img, (uno, dos, tres), (cuatro, cinco, seis))

        elif funcion == 'rotate_colors':
            increment = int(sys.argv[contador])
            contador += 1
            img = rotate_colors(img, increment)

        elif funcion == 'shift':
            horizontal = int(sys.argv[contador])
            vertical = int(sys.argv[contador+1])
            contador += 2
            img = shift(img, horizontal, vertical)

        elif funcion == 'crop':
            x = int(sys.argv[contador])
            y = int(sys.argv[contador+1])
            width = int(sys.argv[contador+2])
            height = int(sys.argv[contador+3])
            contador += 4
            img = crop(img, x, y, width, height)

        elif funcion == 'filter':
            r = float(sys.argv[contador])
            g = float(sys.argv[contador+1])
            b = float(sys.argv[contador+2])
            contador += 3
            img = filter(img, r, g, b)

        else:
            print('Introduzca una de las funciones sugeridas: \n'
                  '- rotate_right\n'
                  '- mirror\n'
                  '- blur\n'
                  '- grayscale'
                  '- change_colors\n'
                  '- rotate_colors\n'
                  '- shift\n'
                  '- crop\n'
                  '- filter\n')
            return

        if contador == len(sys.argv):

            break

    write_img(img, f"{filename}_trans{file_extension}")
    print(f"Transformación aplicada: {funcion}")
    print(f"Imagen resultante guardada como: {filename}_trans{file_extension}")
    Image.open(f"{filename}_trans{file_extension}").show()


if __name__ == '__main__':
    main()