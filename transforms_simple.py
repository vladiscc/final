import sys
from transforms import *
from images import read_img, write_img
from PIL import Image

def split_filename_extension(nombre_imagen):
    parts = nombre_imagen.rsplit('.', 1)

    if len(parts) > 1:
        filename, file_extension = parts
        file_extension = '.' + file_extension
    else:
        filename = nombre_imagen
        file_extension = ''

    return filename, file_extension


def main():
    nombre_imagen = sys.argv[1]
    funcion = sys.argv[2]

    filename, file_extension = split_filename_extension(nombre_imagen)
    print(f"Nombre del archivo: {filename}")
    print(f"Extensión del archivo: {file_extension}")

    img = read_img(nombre_imagen)

    if funcion == 'rotate_right':
        a = rotate_right(img)
    elif funcion == 'mirror':
        a = mirror(img)
    elif funcion == 'blur':
        a = blur(img)
    elif funcion == 'grayscale':
        a = grayscale(img)
    else:
        print('Introduzca una de las funciones sugeridas: \n'
              '- rotate_right\n'
              '- mirror\n'
              '- blur\n'
              '- grayscale')
        return


    write_img(a, f"{filename}_trans{file_extension}")
    print(f"Transformación aplicada: {funcion}")
    print(f"Imagen resultante guardada como: {filename}_trans{file_extension}")
    Image.open(f"{filename}_trans{file_extension}").show()

if __name__ == '__main__':
    main()